
| _HERRAMIENTAS_ | _LENGUAJES Y FRAMEWORKS_ | _TECNICAS_|_PLATAFORMA_|
| -------- | -------- || -------- | -------- |
| **ADOPTAR**||||
|*Eclipse* | *java*| *TDD* | *Windows 10* |  
| *Sublime Studio*|*Css* |*Programacion Orientadas a objetos* ||
||*Haskell*|*Programacion Estructurada*||
||*HTML*|| |
||*Angular*|| |
||*SQL*|||
|**PROBAR**|
|*Android Studio*|*Python*|*Programacion de a pares*|*Linux*|
|*Pycham*|*C*|*Programacion Concurrente*||
|**EVALUAR**|
|*Codeblocks*|*Ruby*|*Programacion Funsional*||
||*C++*|||
|**ESPERAR**|
|*Neo4J*|*Erlang*||*Mac*||
||*CYPHER*|||
||*PHP*|||
||*Asembler*|||












